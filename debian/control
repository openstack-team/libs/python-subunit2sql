Source: python-subunit2sql
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 alembic,
 python3-alembic,
 python3-coverage,
 python3-dateutil,
 python3-fixtures,
 python3-hacking,
 python3-mock,
 python3-openstackdocstheme,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.db,
 python3-psycopg2,
 python3-pymysql,
 python3-six,
 python3-sqlalchemy,
 python3-stestr,
 python3-stevedore,
 python3-testresources,
 python3-testscenarios,
 python3-testtools,
 subunit,
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/openstack-team/libs/python-subunit2sql
Vcs-Git: https://salsa.debian.org/openstack-team/libs/python-subunit2sql.git
Homepage: https://github.com/openstack-infra/subunit2sql

Package: python-subunit2sql-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: subunit file/stream to DB - doc
 subunit2SQL is a tool for storing test results data in a SQL database. Like
 it's name implies it was originally designed around converting subunit streams
 to data in a SQL database and the packaged utilities assume a subunit stream
 as the input format. However, the data model used for the DB does not preclude
 using any test result format. Additionally the analysis tooling built on top
 of a database is data format agnostic. However if you choose to use a
 different result format as an input for the database additional tooling using
 the DB API would need to be created to parse a different test result output
 format. It's also worth pointing out that subunit has several language library
 bindings available. So as a user you could create a small filter to convert a
 different format to subunit. Creating a filter should be fairly easy and then
 you don't have to worry about writing a tool like :ref:`subunit2sql` to use a
 different format.
 .
 For multiple distributed test runs that are generating subunit output it is
 useful to store the results in a unified repository. This is the motivation for
 the testrepository project which does a good job for centralizing the results
 from multiple test runs.
 .
 However, imagine something like the OpenStack CI system where the same basic
 test suite is normally run several hundreds of times a day. To provide useful
 introspection on the data from those runs and to build trends over time the
 test results need to be stored in a format that allows for easy querying.
 Using a SQL database makes a lot of sense for doing this, which was the
 original motivation for the project.
 .
 At a high level subunit2SQL uses alembic migrations to setup a DB schema that
 can then be used by the subunit2sql tool to parse subunit streams and populate
 the DB. Then there are tools for interacting with the stored data in the
 subunit2sql-graph command as well as the sql2subunit command to create a
 subunit stream from data in the database. Additionally, subunit2sql provides a
 Python DB API that can be used to query information from the stored data to
 build other tooling.
 .
 This package contains the documentation.

Package: python3-subunit2sql
Architecture: all
Depends:
 alembic,
 python3-alembic,
 python3-dateutil,
 python3-oslo.config,
 python3-oslo.db,
 python3-pbr,
 python3-six,
 python3-sqlalchemy,
 python3-stevedore,
 subunit,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-subunit2sql-doc,
Description: subunit file/stream to DB - Python 3.x
 subunit2SQL is a tool for storing test results data in a SQL database. Like
 it's name implies it was originally designed around converting subunit streams
 to data in a SQL database and the packaged utilities assume a subunit stream
 as the input format. However, the data model used for the DB does not preclude
 using any test result format. Additionally the analysis tooling built on top
 of a database is data format agnostic. However if you choose to use a
 different result format as an input for the database additional tooling using
 the DB API would need to be created to parse a different test result output
 format. It's also worth pointing out that subunit has several language library
 bindings available. So as a user you could create a small filter to convert a
 different format to subunit. Creating a filter should be fairly easy and then
 you don't have to worry about writing a tool like :ref:`subunit2sql` to use a
 different format.
 .
 For multiple distributed test runs that are generating subunit output it is
 useful to store the results in a unified repository. This is the motivation for
 the testrepository project which does a good job for centralizing the results
 from multiple test runs.
 .
 However, imagine something like the OpenStack CI system where the same basic
 test suite is normally run several hundreds of times a day. To provide useful
 introspection on the data from those runs and to build trends over time the
 test results need to be stored in a format that allows for easy querying.
 Using a SQL database makes a lot of sense for doing this, which was the
 original motivation for the project.
 .
 At a high level subunit2SQL uses alembic migrations to setup a DB schema that
 can then be used by the subunit2sql tool to parse subunit streams and populate
 the DB. Then there are tools for interacting with the stored data in the
 subunit2sql-graph command as well as the sql2subunit command to create a
 subunit stream from data in the database. Additionally, subunit2sql provides a
 Python DB API that can be used to query information from the stored data to
 build other tooling.
 .
 This package contains the Python 3.x module.
